import React, { StrictMode } from 'react';
import ReactDOM from 'react-dom';
import 'semantic-ui-less/semantic.less';

ReactDOM.render(
    <StrictMode></StrictMode>,
    document.getElementById('root')
);